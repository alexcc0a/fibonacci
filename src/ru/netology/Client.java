package com.nesterov;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {  // Объявление класса Client.
    public static void main(String[] args) throws IOException {  // Объявление метода main.
        Socket socket = new Socket("127.0.0.1", 13301);  // Создание сокета для подключения к серверу на локальной машине (127.0.0.1) и порту 13301.

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));  // Создание BufferedReader для чтения данных от сервера.
             PrintWriter out = new PrintWriter(
                     new OutputStreamWriter(socket.getOutputStream()), true);  // Создание PrintWriter для отправки данных серверу.
             Scanner scanner = new Scanner(System.in)) {  // Создание Scanner для ввода данных с клавиатуры.
            String msg;  // Объявление переменной для хранения сообщения.
            while (true) {  // Бесконечный цикл для взаимодействия с сервером.
                System.out.println("Введите число или end для завершения.");  // Вывод приглашения к вводу.
                msg = scanner.nextLine();  // Чтение сообщения с клавиатуры.
                out.println(msg);  // Отправка сообщения серверу.
                if ("end".equals(msg)) {  // Если сообщение "end", завершаем цикл.
                    break;
                }

                System.out.println("Server: " + in.readLine());  // Вывод ответа сервера.
            }
        }
    }
}
