package com.nesterov;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(13301);  // Создание серверного сокета, прослушивающего порт 13301.
        System.out.println("Server started!");  // Вывод сообщения о запуске сервера.
        while (true) {  // Бесконечный цикл для ожидания подключений.
            try (Socket socket = serverSocket.accept();  // Принятие подключения от клиента и создание сокета для общения с клиентом.
                 PrintWriter out = new PrintWriter(socket.getOutputStream(), true);  // Создание PrintWriter для отправки данных клиенту.
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {  // Создание BufferedReader для чтения данных от клиента.
                String msg;  // Объявление переменной для хранения сообщения от клиента.
                while ((msg = in.readLine()) != null) {  // Чтение сообщения от клиента, пока оно не пустое.
                    int n = Integer.parseInt(msg.trim());  // Преобразование сообщения в целое число.

                    int result = findNFibonacciLight(n);  // Вычисление N-го числа Фибоначчи с использованием метода findNFibonacciLight.
                    out.println("Result: " + result);  // Отправка результата клиенту.

                    if (msg.equals("end")) {  // Если клиент отправил "end", завершаем цикл.
                        break;
                    }
                }
            } catch (IOException e) {  // Обработка исключений ввода/вывода.
                e.printStackTrace();  // Вывод информации об исключении.
            }
        }
    }

    static int findNFibonacci(int n) {  // Объявление статического метода для вычисления N-го числа Фибоначчи.
        if (n == 0) {  // Если N равно 0, возвращаем 0.
            return n;
        }
        if (n == 1) {  // Если N равно 1, возвращаем 1.
            return n;
        }
        return findNFibonacci(n - 1) + findNFibonacci(n - 2);  // Рекурсивное вычисление N-го числа Фибоначчи.
    }

    static int findNFibonacciLight(int n) {  // Объявление статического метода для вычисления N-го числа Фибоначчи.
        int a = 0;  // Инициализация переменной a.
        int b = 1;  // Инициализация переменной b.
        int fibonacci = a + b;  // Инициализация переменной fibonacci.
        if (n == 0 || n == 1) {  // Если N равно 0 или 1, возвращаем N.
            return n;
        }
        while (n > 2) {  // Цикл вычисления N-го числа Фибоначчи.
            a = b;  // Обновление значения a.
            b = fibonacci;  // Обновление значения b.
            fibonacci = a + b;  // Вычисление нового значения для fibonacci.
            n--;  // Уменьшение N.
        }
        return fibonacci;  // Возвращение результата.
    }
}
